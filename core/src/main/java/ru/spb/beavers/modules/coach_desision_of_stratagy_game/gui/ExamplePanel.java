package ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui;

import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.ElementKord;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.GroupableTableHeader;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.datainp;
import ru.spb.beavers.modules.coach_desision_of_stratagy_game.gui.rabsourse.PanelEl;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.math.BigDecimal;

/**
 * Created by Sasha on 09.04.2015.
 */
public class ExamplePanel {
    private datainp dt;

    private JTable table;
    private int r;
    private JPanel pn;
    DefaultTableModel dm;
    JScrollPane scroll1;
    ElementKord last_el_coords;
    private JLabel description_area;
    private String algorithm_description;
    public ExamplePanel(JPanel panel, datainp tr)
    {
        dt=tr;
        pn=panel;
        System.out.print(tr.getrdraw(0));

        last_el_coords = new ElementKord();
        panel.setLayout(null);
        panel.setSize(760, 1700);
        panel.setPreferredSize(new Dimension(750, 1700));

        createDecisionTable(panel);
        createButtonPanel(panel);
        createDecisionTextPane(panel);

        /*root_node = root;
        initTableFromTree(root);
        algorithm = new Algorithm(root);

        algorithm.addComponent(new IStepLogger() {
            @Override
            public void logStep(String str) {
                algorithm_description += str;
                System.out.println(str);
                description_area.setText("<html><body width=\"580\" height=\"500\">"
                        + algorithm_description
                        + "</body></html>");
            }
        });
        createDecisionTree(panel, root_node);*/
    }

    public JPanel getPanel()
    {
        return pn;
    }
    private void createDecisionTable(JPanel panel)
    {

        PanelEl.setTextHeader("Таблица оптимальных решений", 10, 400, 20, last_el_coords, panel);
         dm = new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return (column == 0 || column == 1 || column==2|| column==3 || column==4) ? false : true;
            }
        };
        System.out.print(dt.getrdes(0));
        setnewtable();
                 table = new JTable( dm ) {
            DefaultTableCellRenderer renderRight = new DefaultTableCellRenderer();

            {
                renderRight.setHorizontalAlignment(SwingConstants.CENTER);
            }

            @Override
            public TableCellRenderer getCellRenderer (int arg0, int arg1) {
                return renderRight;
            }

            protected JTableHeader createDefaultTableHeader() {
                return new GroupableTableHeader(columnModel);
            }
        };
        TableColumnModel cm = table.getColumnModel();
        /*cm.getColumn(0).setMaxWidth(80);
        ColumnGroup g_1 = new ColumnGroup("Стоимость ветви");
        g_1.add(cm.getColumn(3));
        g_1.add(cm.getColumn(4));
        ColumnGroup g_2 = new ColumnGroup("Ожидаемый доход в узле");
        g_2.add(cm.getColumn(5));
        g_2.add(cm.getColumn(6));

        GroupableTableHeader header = (GroupableTableHeader)table.getTableHeader();
        header.addColumnGroup(g_1);
        header.addColumnGroup(g_2);*/

        table.setPreferredSize(new Dimension(700, table.getRowHeight() * 10));
        table.setSize(700, table.getRowHeight() * 10);
        JScrollPane scroll = new JScrollPane(table);
        scroll.setSize(700, 278);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(10, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - table.getWidth() / 2, last_el_coords.y);
        panel.add(scroll);
        pn=panel;
       // scroll1=scroll;

    }

    private void createButtonPanel(final JPanel panel) {
        JPanel jp = new JPanel();
        last_el_coords.updateValues(10, 35);
        jp.setSize(580, last_el_coords.height);
        jp.setLocation(panel.getWidth() / 2 - jp.getWidth() / 2, last_el_coords.y);
        JButton reset_btn = new JButton("Сбросить");
        reset_btn.setSize(150, 30);
        reset_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                returnDefaultTable();
                algorithm_description = "";
                description_area.setText("<html><body width=\"580\" height=\"500\">"
                        + algorithm_description
                        + "</body></html>");
                System.out.print("del");
            }
        });
        jp.add(reset_btn);
        JButton one_step_btn = new JButton("Сохранить  решение в файл");
        one_step_btn.setSize(150, 30);
        one_step_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                algorithm_description=textSolution();
                JFileChooser fc=new JFileChooser();
                int result = fc.showSaveDialog(null);
                if(result == JFileChooser.APPROVE_OPTION)
                {
                    try{
                        File file = new File(fc.getSelectedFile()+".html");
                        BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                                new FileOutputStream(file), "cp1251"
                        ));
                        out.write(algorithm_description);
                        out.flush();
                        out.close();
                    }
                    catch(IOException io)
                    {
                        // System.exit(1);
                    }
                }
               // DefaultTableModel tableModel=(DefaultTableModel) table.getModel();
                //tableModel.fireTableDataChanged();
               // pn.repaint();
                //createDecisionTable( pn);
                //panel.get
                System.out.print("onestep");//algorithm.proceedOneStep();
            }
        });
        jp.add(one_step_btn);
        JButton all_steps_btn = new JButton("Посчитать всё");
        all_steps_btn.setSize(150, 30);
        all_steps_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.out.print("all");//algorithm.solve();
                solution();
                algorithm_description=textSolution();
                description_area.setText("<html><body width=\"580\" height=\"500\">"
                        + algorithm_description
                        + "</body></html>");
            }
        });
        jp.add(all_steps_btn);

        reset_btn.setLocation(jp.getWidth() / 2 - (20 * 2 + reset_btn.getWidth()
                + one_step_btn.getWidth() + all_steps_btn.getWidth()) / 2, 0);
        one_step_btn.setLocation(reset_btn.getX() + reset_btn.getWidth() + 20, 0);
        all_steps_btn.setLocation(all_steps_btn.getX() + all_steps_btn.getWidth() + 20, 0);

        panel.add(jp);
    }
    private void createDecisionTextPane(JPanel panel)
    {
        PanelEl.setTextHeader("Описание решения", 30, 300, 20, last_el_coords, panel);
        //algorithm_description = "dfdfdf<p>dsds</p>";
        //String dsd="Привет коля";
        algorithm_description=" ";
        description_area = new JLabel("<html><body width=\"580\" height=\"500\">"
                + algorithm_description
                + "</body></html>");
        //description_area.setSize(580, 1400);
       // description_area.setPreferredSize(new Dimension(580, 1400));
        JScrollPane scroll = new JScrollPane(description_area);
        scroll.setSize(600, 400);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        last_el_coords.updateValues(10, scroll.getHeight());
        scroll.setLocation(panel.getWidth() / 2 - scroll.getWidth() / 2, last_el_coords.y);
        panel.add(scroll);
    }

    public void solution()
    {
        for(int i=0; i<10; i++)
        {
            //System.out.print(3);
            double uDg=dt.getrdraw(i)*dt.getpg(i)+dt.getrlose(i)*(1-dt.getpg(i));
            double uDt=dt.getrwin(i)*dt.getpt(i)+dt.getrlose(i)*(1-dt.getpt(i));
            if(uDt>uDg) dt.ubddes(i, "d2"); else dt.ubddes(i, "d1");
            if(dt.getrdraw(i)>dt.getrlose(i)) dt.ubddeso(i, "d1"); else dt.ubddeso(i, "d2");
            settable();
        }
       // System.out.print(dt.getrdes(0));
    }

    public void returnDefaultTable()
    {
        datainp defaultdt=new datainp();
        dt=defaultdt;
        settable();
    }

    public String textSolution()
    {   String textsol="Расчет полезностей решений " +
            "<p>R( y1, d1) = r(a1) = r1;\n" +
            "<p>R( y2, d1)= r(a2)= r2;</p> <p>R( y1, d2 )= r(a3) = r3;</p> <p>R( y2,d2 )=  r(a4 ) = r2 .</p>\n" +
            "<p>Полезность решения d1 (лотереи L1) равна u(d1) = p11r1 + p21r2 .</p><p> Полез\n" +
            "ность решения d2 (лотереи L2 ) равна u(d2 ) = p12r3 + p22r1.</p></p><p></p>";
        for(int i=0; i<10; i++)
        {
            textsol=textsol+" <p>"+String.valueOf(i)+".1)  u(d1)="+String.valueOf(dt.getpg(i))+"*"+String.valueOf(dt.getrdraw(i))+"+"+String.valueOf(BigDecimal.valueOf(1.0-dt.getpg(i)))+"*"+String.valueOf(dt.getrlose(i))+"="+String.valueOf(BigDecimal.valueOf(dt.getrdraw(i)*dt.getpg(i)+dt.getrlose(i)*(roundResult(1-dt.getpg(i),2))))+"</p>";
            textsol=textsol+" <p>"+String.valueOf(i)+".2)  u(d2)="+String.valueOf(dt.getpt(i))+"*"+String.valueOf(dt.getrwin(i))+"+"+String.valueOf(BigDecimal.valueOf(1.0 - dt.getpt(i)))+"*"+String.valueOf(dt.getrlose(i))+"="+String.valueOf(BigDecimal.valueOf(dt.getrwin(i) * dt.getpt(i) + dt.getrlose(i) * (roundResult(1 - dt.getpt(i), 2))))+"</p>";
            double uDg=dt.getrdraw(i)*dt.getpg(i)+dt.getrlose(i)*(1-dt.getpg(i));
            double uDt=dt.getrwin(i)*dt.getpt(i)+dt.getrlose(i)*(1-dt.getpt(i));
            if(uDt>uDg) textsol=textsol+"<p>  u(d2)>d(d1) = > Наилучшим решением по Байесовскому методу является решение d2 т.е. забить тачдаун.</p>";
            else
                textsol=textsol+"<p>  u(d1)>d(d2) = > Наилучшим решением по Байесовскому методу является решение d1 т.е. забить гол.</p>";
            textsol=textsol+"<p></p>";
        }
        return textsol;
    }
    public String textSolutiontxt()
    {   String textsol="Расчет полезностей решений " +
            "R( y1, d1) = r(a1) = r1;\n" +
            "R( y2, d1)= r(a2)= r2;\n R( y1, d2 )= r(a3) = r3;\n R( y2,d2 )=  r(a4 ) = r2 .\n" +
            "Полезность решения d1 (лотереи L1) равна u(d1) = p11r1 + p21r2 .\n Полез" +
            "ность решения d2 (лотереи L2 ) равна u(d2 ) = p12r3 + p22r1.\n\n";
        for(int i=0; i<10; i++)
        {
            textsol=textsol+String.valueOf(i)+".1)  u(d1)="+String.valueOf(dt.getpg(i))+"*"+String.valueOf(dt.getrdraw(i))+"+"+String.valueOf(BigDecimal.valueOf(1.0-dt.getpg(i)))+"*"+String.valueOf(dt.getrlose(i))+"="+String.valueOf(BigDecimal.valueOf(dt.getrdraw(i)*dt.getpg(i)+dt.getrlose(i)*(roundResult(1-dt.getpg(i),2))))+"\n";
            textsol=textsol+String.valueOf(i)+".2)  u(d2)="+String.valueOf(dt.getpt(i))+"*"+String.valueOf(dt.getrwin(i))+"+"+String.valueOf(BigDecimal.valueOf(1.0 - dt.getpt(i)))+"*"+String.valueOf(dt.getrlose(i))+"="+String.valueOf(BigDecimal.valueOf(dt.getrwin(i) * dt.getpt(i) + dt.getrlose(i) * (roundResult(1 - dt.getpt(i), 2))))+"\n";
            double uDg=dt.getrdraw(i)*dt.getpg(i)+dt.getrlose(i)*(1-dt.getpg(i));
            double uDt=dt.getrwin(i)*dt.getpt(i)+dt.getrlose(i)*(1-dt.getpt(i));
            if(uDt>uDg) textsol=textsol+"  u(d2)>d(d1) = > Наилучшим решением по Байесовскому методу является решение d2 т.е. забить тачдаун.\n";
            else
                textsol=textsol+"  u(d1)>d(d2) = > Наилучшим решением по Байесовскому методу является решение d1 т.е. забить гол.\n";
            textsol=textsol+"\n";
        }
        return textsol;
    }

   public double roundResult (double d, int precise) {


       //System.out.print(String.valueOf(BigDeci.valueOf(1)-dt.getpt(0))));
        precise = 10^precise;
        d = d*precise;
        int i = (int) Math.round(d);
        return (double) i/precise;

    }

    public void settable()
    {
        dm.setDataVector(new Object[][]{
                        {String.valueOf(dt.getpg(0)), String.valueOf(dt.getpt(0)), String.valueOf(dt.getrwin(0)),  String.valueOf(dt.getrlose(0)),  String.valueOf(dt.getrdraw(0)), dt.getrdes(0) },
                        {String.valueOf(dt.getpg(1)), String.valueOf(dt.getpt(1)), String.valueOf(dt.getrwin(1)),  String.valueOf(dt.getrlose(1)),  String.valueOf(dt.getrdraw(1)),  dt.getrdes(1)},
                        {String.valueOf(dt.getpg(2)), String.valueOf(dt.getpt(2)), String.valueOf(dt.getrwin(2)),  String.valueOf(dt.getrlose(2)),  String.valueOf(dt.getrdraw(2)),  dt.getrdes(2)},
                        {String.valueOf(dt.getpg(3)), String.valueOf(dt.getpt(3)), String.valueOf(dt.getrwin(3)),  String.valueOf(dt.getrlose(3)),  String.valueOf(dt.getrdraw(3)),  dt.getrdes(3)},
                        {String.valueOf(dt.getpg(4)), String.valueOf(dt.getpt(4)), String.valueOf(dt.getrwin(4)),  String.valueOf(dt.getrlose(4)),  String.valueOf(dt.getrdraw(4)),  dt.getrdes(4)},
                        {String.valueOf(dt.getpg(5)), String.valueOf(dt.getpt(5)), String.valueOf(dt.getrwin(5)),  String.valueOf(dt.getrlose(5)),  String.valueOf(dt.getrdraw(5)),  dt.getrdes(5)},
                        {String.valueOf(dt.getpg(6)), String.valueOf(dt.getpt(6)), String.valueOf(dt.getrwin(6)),  String.valueOf(dt.getrlose(6)),  String.valueOf(dt.getrdraw(6)),  dt.getrdes(6)},
                        {String.valueOf(dt.getpg(7)), String.valueOf(dt.getpt(7)), String.valueOf(dt.getrwin(7)),  String.valueOf(dt.getrlose(7)),  String.valueOf(dt.getrdraw(7)),  dt.getrdes(7)},
                        {String.valueOf(dt.getpg(8)), String.valueOf(dt.getpt(8)), String.valueOf(dt.getrwin(8)),  String.valueOf(dt.getrlose(8)),  String.valueOf(dt.getrdraw(8)),  dt.getrdes(8)},
                        {String.valueOf(dt.getpg(9)), String.valueOf(dt.getpt(9)), String.valueOf(dt.getrwin(9)),  String.valueOf(dt.getrlose(9)),  String.valueOf(dt.getrdraw(9)),  dt.getrdes(9)},

                },
                new Object[]{"Веротность\n забить гол", "Вероятность\n забить тачдаун", "Рейтинг в случае\nпобеды", "Рейтинг в случае\n парожаения", "Рейтинг в случае\n ничьи",  "Оптимальноерешение\nБайесовский подход"});

    }
    public void setnewtable()
    {
        dm.setDataVector(new Object[][]{
                        {"-", "-", "-", "-",  "-", "-" },
                        {"-", "-", "-", "-",  "-", "-" },
                        {"-", "-", "-", "-",  "-", "-" },
                        {"-", "-", "-", "-",  "-", "-" },
                        {"-", "-", "-", "-",  "-", "-" },
                        {"-", "-", "-", "-",  "-", "-" },
                        {"-", "-", "-", "-",  "-", "-" },
                        {"-", "-", "-", "-",  "-", "-" },
                        {"-", "-", "-", "-",  "-", "-" },
                        {"-", "-", "-", "-",  "-", "-" },

                },
                new Object[]{"Веротность\n забить гол", "Вероятность\n забить тачдаун", "Рейтинг в случае\nпобеды", "Рейтинг в случае\n парожаения", "Рейтинг в случае\n ничьи",  "Оптимальноерешение\nБайесовский подход"});

    }


}
