package ru.spb.beavers.modules.hotelrooms.GUI.Utility;

public class LastElCoords
{
    public int y;
    public int height;

    public LastElCoords()
    {
        this.y = 0;
        this.height = 0;
    }

    public LastElCoords(int y, int height)
    {
        this.y = y;
        this.height = height;
    }

    public void updateValues(int oy, int height)
    {
        this.y = this.y + this.height + oy;
        this.height = height;
    }
};