package ru.spb.beavers.modules;


import ru.spb.beavers.modules.task_3_0_x_components.graph.GraphContainer;
import ru.spb.beavers.modules.task_3_0_x_components.panels.InputPanelBuilder;
import ru.spb.beavers.modules.task_3_0_x_components.panels.ResolveExamplePanelBuilder;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by Vladimir_ermakov on 4/11/2015.
 */
public class TaskModule_3_0_6 implements ITaskModule{

    public TaskModule_3_0_6(){
        GraphContainer.getInstance();
    }

    @Override
    public String getTitle() {
        return "Задача 3.0.6. Полупорядок.";
    }

    @Override
    public void initDescriptionPanel(JPanel panel) {
        panel.removeAll();
        Image image = null;
        try {
            image = ImageIO.read(TaskModule_3_0_6.class.getResource("task_3_x_resources/3_0_6/3_1_description.JPG"));
        } catch (IOException e) {
        }
        panel.add(new JLabel(new ImageIcon(image)));

    }


    @Override
    public void initSolutionPanel(JPanel panel) {
        panel.removeAll();
        Image image = null;
        try {
            image = ImageIO.read(TaskModule_3_0_6.class.getResource("task_3_x_resources/3_0_6/3_1_resources.JPG"));
        } catch (IOException e) {
        }
        panel.add(new JLabel(new ImageIcon(image)));

    }

    @Override
    public void initInputPanel(JPanel panel) {
        panel.removeAll();
        InputPanelBuilder panelConstructor = new InputPanelBuilder();
        panelConstructor.initPanel(panel);
    }

    @Override
    public void initExamplePanel(JPanel panel) {
        panel.removeAll();
        ResolveExamplePanelBuilder panelConstructor = new ResolveExamplePanelBuilder();
        panelConstructor.initPanel(panel);

    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        return null;
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        return null;
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        return null;
    }


}
