package ru.spb.beavers.modules;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileNotFoundException;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import ru.spb.beavers.modules.betting_on_outcome_of_match.Constants;
import info.monitorenter.gui.chart.Chart2D;
import info.monitorenter.gui.chart.IAxis;
import info.monitorenter.gui.chart.ITrace2D;
import info.monitorenter.gui.chart.traces.Trace2DSimple;

public class BettingOnOutcomeOfMatchModule implements ITaskModule {
    private final JTextField pkTextField;
    private final JLabel pkLabel;
    private final JTextField p11TextField;
    private final JTextField p12TextField;
    private final JCheckBox  hasAddInfo;
    private JPanel mainContext;
    private float p11;
    private float p12;
    private float p21;
    private float p22;
    private float uy1d1;
    private float uy2d1;
    private float ud1;
    private float pdef;
    private float pk;

    private boolean isValidTextValue(String text) {
        Pattern p = Pattern.compile("\\-?\\d+(\\.\\d{0,})?", Pattern.UNICODE_CASE);
        return p.split(text).length == 0;
    }
    private boolean checkProbability() {
        if(hasAddInfo.isSelected()) {
            pk = Float.parseFloat(pkTextField.getText());
            if(pk < 0 || pk > 1) return false;
        }
        p11 = Float.parseFloat(p11TextField.getText());
        p12 = Float.parseFloat(p12TextField.getText());
        if(p11 < 0 || p12 < 0 || p11 > 1 || p12 > 1) return false;
        return true;
    }

    private boolean checkParms() {
        if(!isValidTextValue(p11TextField.getText())) return false;
        if(!isValidTextValue(p12TextField.getText())) return false;
        if(hasAddInfo.isSelected() && !isValidTextValue(pkTextField.getText()))
            return false;
        return checkProbability();
    }

    private void setUpAddParmField() {
        mainContext.add(pkLabel);
        mainContext.add(pkTextField);
        mainContext.updateUI();
    }

    private void removeAddParmField() {
        mainContext.remove(pkLabel);
        mainContext.remove(pkTextField);
        mainContext.updateUI();
    }

    private String modifyExampleLine(String line) {
        String [] items = line.split(" ");
        String result = "";
        for(String item : items) {
            switch(item) {
                case "p11": result += p11 + " "; break;
                case "p12": result += p12 + " "; break;
                case "p21": result += p21 + " "; break;
                case "p22": result += p22 + " "; break;
                case "uy1d1": result += uy1d1 + " "; break;
                case "uy2d1": result += uy2d1 + " "; break;
                case "pdef": result += pdef + " "; break;
                case "pk": result += pk + " "; break;
                case "ud1": result += ud1 + " "; break;
                default: result += item + " ";
            }
        }
        return result;
    }

    private void setUpExampleWithAddInfo(JPanel panel) {
        p11 = Float.parseFloat(p11TextField.getText());
        p12 = Float.parseFloat(p12TextField.getText());
        pk = Float.parseFloat(pkTextField.getText());
        p21 = 1 - p11;
        p22 = 1 - p12;
        ud1 = (float) (pk * (1.25 * p11 - 1) + (1 - pk) * (3 * p12 - 1));
        pdef = 5 * p11 / 12;

        String exampleText = "";
        // Get template
        try {
            URL resource = ClassLoader.getSystemClassLoader().getResource(
                    Constants.RESOURCE_PATH + Constants.EXAPMLE_WITH_ADD_INFO
            );
            assert resource != null;
            Path path = Paths.get(resource.toURI());
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for (String line : lines) exampleText += modifyExampleLine(line);
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(BettingOnOutcomeOfMatchModule.class.getName()).log(Level.SEVERE, null, ex);
        }

        exampleText += getResultTailWithAddInfo();
        panel.add(new JLabel(exampleText));
        JPanel chart = getChart();
        panel.add(chart);
    }

    String getResultTailWithAddInfo() {
        String tail = "<b>Результат:</b><br/>";
        if(ud1 > 0) {
            tail +=
                    "Нэнси заключит пари, так как ожидает для себя неотрицательный исход " +
                            "(u(d<sub>1</sub> = " + ud1 + ").<br/><br/>";
        } else {
            tail += "Заключать пари для Нэнси не выгодно, поэтому она откажется<br/><br/>";
        }
        tail += "</p><br/><br/><b>График полезности точной информации</b><br/><br/></html>";
        return tail;
    }

    String getResultTailWithoutAddInfo() {
        String tail = "<b>Результат:</b><br/>";
        if(uy1d1 <= 0 && uy2d1 <= 0) {
            tail += "Заключать пари для Нэнси не выгодно, поэтому она откажется<br/><br/>";
        } else if(uy1d1 >= 0 && uy2d1 >= 0) {
            if(uy1d1 == uy2d1) {
                tail +=
                        "Нэнси согласится заключить пари в любом случае, т. к. ценности исходов " +
                                "равны (" + uy1d1 + " = " + uy2d1 + ").<br/><br/>";
            } else if(uy1d1 > uy2d1) {
                tail +=
                        "Нэнси согласится заключить пари в любом случае, но для нее будет " +
                                "предпочтительнее, если тренер примет решение попытаться забить гол,<br/> т. к. " +
                                "в этом случае ценность исхода будет больше (" + uy1d1 + " > " + uy2d1 + ").<br/><br/>";
            } else if(uy1d1 < uy2d1){
                tail +=
                        "Нэнси согласится заключить пари в любом случае, но для нее будет " +
                                "предпочтительнее, если тренер примет решение попытаться совершить<br/> тачдаун, т. к. " +
                                "в этом случае ценность исхода будет больше (" + uy1d1 + " < " + uy2d1 + ").<br/><br/>";
            }
        } else if(uy1d1 > uy2d1) {
            tail +=
                    "Нэнси согласится заключить пари только в том случае, если тренер примет " +
                            "решение попытаться забить гол, т. к. только в этом случае ценность <br/>" +
                            "исхода положительна (" + uy1d1 + ").<br/><br/>";
        } else if(uy1d1 < uy2d1) {
            tail +=
                    "Нэнси согласится заключить пари только в том случае, если тренер примет " +
                            "решение попытаться совершить тачдаун, т. к. только в этом случае<br/> ценность " +
                            "исхода положительна (" + uy2d1 + ").<br/><br/>";
        }
        tail += "</p></html>";
        return tail;
    }

    private float calcDU(float pk) {
        return pk > 0.68f ? 0.1875f * pk - (0.5875f * pk - 0.4f) : 0.1875f * pk;
    }

    private JPanel getChart() {
        Chart2D staticChart = new Chart2D();
        ITrace2D trace = new Trace2DSimple();
        trace.setName("Полезность точной информации");

        staticChart.setBackground(Color.lightGray);

        IAxis axisX = staticChart.getAxisX();
        axisX.setPaintGrid(true);
        IAxis axisY = staticChart.getAxisY();
        axisY.setPaintGrid(true);

        staticChart.addTrace(trace);
        for (float i = 1f; i >= 0f; i -= Constants.CHART_STEP) {
            trace.addPoint(i, calcDU(i));
        }
        staticChart.setMinimumSize(Constants.CHART_SIZE);
        staticChart.setPreferredSize(Constants.CHART_SIZE);
        staticChart.setMaximumSize(Constants.CHART_SIZE);
        JPanel panel = new JPanel();
        panel.add(staticChart);
        return panel;
    }

    private void setUpExampleWithoutAddInfo(JPanel panel) {
        p11 = Float.parseFloat(p11TextField.getText());
        p12 = Float.parseFloat(p12TextField.getText());
        p21 = 1 - p11;
        p22 = 1 - p12;
        uy1d1 = (float) (1.25 * p11 - 1);
        uy2d1 = (float) (3 * p12 - 1);
        pdef = 5 * p11 / 12;

        String exampleText = "";
        // Get template
        try {
            URL resource = ClassLoader.getSystemClassLoader().getResource(
                    Constants.RESOURCE_PATH + Constants.EXAPMLE_WITHOUT_ADD_INFO
            );
            assert resource != null;
            Path path = Paths.get(resource.toURI());
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for(String line : lines) exampleText += modifyExampleLine(line);
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(BettingOnOutcomeOfMatchModule.class.getName()).log(Level.SEVERE, null, ex);
        }

        exampleText += getResultTailWithoutAddInfo();
        panel.add(new JLabel(exampleText));
    }

    public BettingOnOutcomeOfMatchModule() {
        mainContext = null;
        pkLabel = new JLabel(Constants.PK_INPUT_STRING);
        pkTextField = new JTextField("0.5");
        p11TextField = new JTextField("0.92");
        p12TextField = new JTextField("0.2");
        hasAddInfo = new JCheckBox(Constants.HAS_ADD_INFO_STRING);

        p11TextField.setMaximumSize(Constants.TEXT_FIELD_SIZE);
        pkTextField.setMaximumSize(Constants.TEXT_FIELD_SIZE);
        p12TextField.setMaximumSize(Constants.TEXT_FIELD_SIZE);

        hasAddInfo.setAlignmentX(Container.CENTER_ALIGNMENT);
        pkLabel.setAlignmentX(Container.CENTER_ALIGNMENT);
        p12TextField.setAlignmentX(Container.CENTER_ALIGNMENT);
        pkTextField.setAlignmentX(Container.CENTER_ALIGNMENT);
        p11TextField.setAlignmentX(Container.CENTER_ALIGNMENT);

        hasAddInfo.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(hasAddInfo.isSelected()) setUpAddParmField();
                else removeAddParmField();
            }
        });
    }

    @Override
    public String getTitle() {
        return Constants.TITLE;
    }

    @Override
    public void initDescriptionPanel(JPanel panel) {
        String descriptionText = "";
        
        // Get the HTML based description text from resources.
        try {
            URL resource = ClassLoader.getSystemClassLoader().getResource(
                Constants.RESOURCE_PATH + Constants.DESCRIPTION_FILE
            );
            assert resource != null;
            Path path = Paths.get(resource.toURI());
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for(String line : lines) descriptionText += line;
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(BettingOnOutcomeOfMatchModule.class.getName()).log(Level.SEVERE, null, ex);
        }
        // Set up description context.
        JLabel descriptionLabel = new JLabel(descriptionText);
        descriptionLabel.setPreferredSize(new Dimension(770,400));
        panel.removeAll();
        panel.setLayout(new GridBagLayout() );
        panel.add(descriptionLabel);
    }

    @Override
    public void initSolutionPanel(JPanel panel) {
        String solutionText1 = "";
        String solutionText2 = "";
        String solutionText3 = "";
        URL resource;
        // Get the HTML based description text from resources.
        try {
            resource = ClassLoader.getSystemClassLoader().getResource(
                Constants.RESOURCE_PATH + Constants.SOLUTION_FILE_PART1
            );
            Path path = Paths.get(resource.toURI());
            List<String> lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for(String line : lines) solutionText1 += line;
            
            resource = ClassLoader.getSystemClassLoader().getResource(
                Constants.RESOURCE_PATH + Constants.SOLUTION_FILE_PART2
            );
            path = Paths.get(resource.toURI());
            lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for(String line : lines) solutionText2 += line;
            
            resource = ClassLoader.getSystemClassLoader().getResource(
                Constants.RESOURCE_PATH + Constants.SOLUTION_FILE_PART3
            );
            path = Paths.get(resource.toURI());
            lines = Files.readAllLines(path, StandardCharsets.UTF_8);
            for(String line : lines) solutionText3 += line;
        } catch (IOException | URISyntaxException ex) {
            Logger.getLogger(BettingOnOutcomeOfMatchModule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // Set up solution context.
        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JLabel solutionTextLabel1 = new JLabel(solutionText1);
        solutionTextLabel1.setMinimumSize(new Dimension(770,600));
        solutionTextLabel1.setMaximumSize(new Dimension(770,600));
        panel.add(solutionTextLabel1);
        
        resource = ClassLoader.getSystemClassLoader().getResource(
            Constants.RESOURCE_PATH + Constants.TREE_WITHOUT_ADD_INFO
        );
        panel.add(new JLabel(new ImageIcon(resource)));
        panel.add(new JLabel(Constants.DIAGRAM_TEXT));
        resource = ClassLoader.getSystemClassLoader().getResource(
            Constants.RESOURCE_PATH + Constants.DIAGRAM_WITHOUT_ADD_INFO
        );
        panel.add(new JLabel(new ImageIcon(resource)));
        JLabel solutionTextLabel2 = new JLabel(solutionText2);
        solutionTextLabel2.setMaximumSize(new Dimension(770,600));
        solutionTextLabel2.setMaximumSize(new Dimension(770,600));
        panel.add(solutionTextLabel2);
        resource = ClassLoader.getSystemClassLoader().getResource(
            Constants.RESOURCE_PATH + Constants.TREE_WITH_ADD_INFO
        );
        panel.add(new JLabel(new ImageIcon(resource)));
        panel.add(new JLabel(Constants.DIAGRAM_TEXT));
        resource = ClassLoader.getSystemClassLoader().getResource(
            Constants.RESOURCE_PATH + Constants.DIAGRAM_WITH_ADD_INFO
        );
        panel.add(new JLabel(new ImageIcon(resource)));
        JLabel solutionTextLabel3 = new JLabel(solutionText3);
        solutionTextLabel3.setMaximumSize( new Dimension(770,400));
        solutionTextLabel3.setMaximumSize(new Dimension(770,400));
        panel.add(solutionTextLabel3);
    }

    @Override
    public void initInputPanel(JPanel panel) {
        mainContext = panel;
        // Make up input panel using existings final gui items.
        panel.removeAll();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JLabel title = new JLabel(Constants.INPUT_PANEL_TITLE);
        title.setHorizontalAlignment(SwingConstants.CENTER);
        title.setAlignmentX(Container.CENTER_ALIGNMENT);
        panel.add(title);
        title = new JLabel(Constants.P11_INPUT_STRING);
        title.setAlignmentX(Container.CENTER_ALIGNMENT);
        panel.add(title);
        panel.add(p11TextField);
        title = new JLabel(Constants.P12_INPUT_STRING);
        title.setAlignmentX(Container.CENTER_ALIGNMENT);
        panel.add(title);
        panel.add(p12TextField);
        panel.add(hasAddInfo);
    }

    @Override
    public void initExamplePanel(JPanel panel) {
        panel.removeAll();
        panel.setLayout(new GridLayout(2, 1));
        // Check if we have valid parms
        if(!checkParms()) {
            JLabel errorText = new JLabel(Constants.PARMS_ERROR_STRING);
            panel.add(errorText);
            return;
        }
        // Set up context
        if(hasAddInfo.isSelected()) setUpExampleWithAddInfo(panel);
        else setUpExampleWithoutAddInfo(panel);
    }

    @Override
    public ActionListener getPressSaveListener() throws IllegalArgumentException {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser saveDialog = new JFileChooser();
                saveDialog.showSaveDialog(null);
                File file = saveDialog.getSelectedFile();
                if(file == null) return;
                PrintWriter out = null;
                try {
                    out = new PrintWriter(file);
                    out.println(pkTextField.getText());
                    out.println(p11TextField.getText());
                    out.println(p12TextField.getText());
                    out.println(hasAddInfo.isSelected());
                    out.close();
                }
                catch (FileNotFoundException ex) { }
                finally {
                    if(out != null) out.close();
                }
            }
        };
    }

    @Override
    public ActionListener getPressLoadListener() throws IllegalArgumentException {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser openDialog = new JFileChooser();
                openDialog.showOpenDialog(null);
                File file = openDialog.getSelectedFile();
                if(file == null) return;
                Scanner sc = null;
                try {
                    sc = new Scanner(file);
                    pkTextField.setText(sc.nextLine());
                    p11TextField.setText(sc.nextLine());
                    p12TextField.setText(sc.nextLine());
                    hasAddInfo.setSelected(sc.nextBoolean());
                }
                catch (FileNotFoundException ex) { }
                finally {
                    if(sc != null) sc.close();
                }
            }
        };
    }

    @Override
    public ActionListener getDefaultValuesListener() throws IllegalArgumentException {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                    pkTextField.setText("0.5");
                    p11TextField.setText("0.98");
                    p12TextField.setText("0.2");
                    hasAddInfo.setSelected(false);
            }
        };
    }
}
