package ru.spb.beavers.core.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * View вывода результатов решаемой задачи
 */
public class ExampleView extends JPanel{

    private final JPanel examplePanel;
    private final JButton btnGoInputs;
    private final JButton btnGoMenu;

    public ExampleView() {
        super(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.0;
        constraints.weighty = 0.1;
        constraints.gridwidth = 3;
        constraints.anchor = GridBagConstraints.CENTER;
        this.add(new JLabel(), constraints);

        btnGoInputs = new JButton("<");
        btnGoInputs.setToolTipText("Назад к вводу исходных данных");
        constraints.fill = GridBagConstraints.NONE;
        constraints.insets = new Insets(0, 5, 0, 5);
        constraints.gridwidth = 1;
        constraints.weightx = 0.0;
        constraints.ipady = 22;
        constraints.gridy = 1;
        this.add(btnGoInputs, constraints);

        btnGoMenu = new JButton(">");
        btnGoMenu.setToolTipText("В меню");
        constraints.gridx = 2;
        this.add(btnGoMenu, constraints);

        examplePanel = new JPanel(new GridBagLayout());
        JScrollPane scrollPane = new JScrollPane(examplePanel);
        scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(0, 0));
        scrollPane.getVerticalScrollBar().setUnitIncrement(16);
        scrollPane.setOpaque(false);
        scrollPane.getViewport().setOpaque(false);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        constraints.fill = GridBagConstraints.BOTH;
        constraints.gridx = 1;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        this.add(scrollPane, constraints);

        constraints.gridy = 2;
        constraints.gridx = 0;
        constraints.weighty = 0;
        constraints.gridwidth = 3;
        this.add(new JLabel(), constraints);

        initListeners();
    }

    private void initListeners() {
        btnGoInputs.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                GUIManager.setActiveView(GUIManager.getInputView());
            }
        });

        btnGoMenu.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                GUIManager.setActiveView(GUIManager.getMenuView());
            }
        });
    }

    public JPanel getExamplePanel() {
        return examplePanel;
    }
}
