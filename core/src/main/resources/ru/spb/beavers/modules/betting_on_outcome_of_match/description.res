<html> 
	<h2 align='center'>Неформальная постановка задачи</h2> 
	<p>     <br/>
		Друзья, Нэнси и Рон, наблюдающие за описанным в 1.4.1 футбольным матчем, собираются 
		заключить пари на исход матча после того, как тренерпримет решение попытаться забить
		гол или же совершить еще один тачдаун. Нэнси предлагает следующие ставки Рону, и он соглашается: <br/>
		– если тренер решится на попытку тачдауна и его команда победит, Рон должен Нэнси $2. 
		  Если же не выиграют, тогда Нэнси платит Рону $1; <br/>
		– если тренер принимает решение попробовать забить гол и команда выигрывает, 
		  Рон платит Нэнси $0.25, если проигрывают, то Нэнси платит $1. <br/><br/>

		Нэнси может заключить пари, не зная решения тренера, либо сразу после того, 
		как узнает, но до того, как исход матча будет известен. 
	</p>   
</html>